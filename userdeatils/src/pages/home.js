import axios from 'axios'

import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import config from '../config'

const Home = () =>{
const [homes, setHomes] = useState([])
const navigate = useNavigate()

  useEffect(() => {
    getUser()
  }, [])

  const getUser = () => {
    axios
      .get(config.serverURL + '/viewUser', {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data

        if (result['status'] === 'success') {
          console.log(result)
          setHomes(result['data'])
        } else {
          toast.error(result['error'])
        }
      })
  }

  // delete user
  const deleteUser = (username) => {
    axios
      .delete(config.serverURL + '/deleteUser/' + username, {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data
        if (result['status'] === 'success') {
            getUser()
        } else {
          toast.error(result['error'])
        }
      })
  }

    // edit user
  const editUser = (username) => {
    navigate('/editUser', { state: { homeUser: username } })
  }

  return (
    <div className='container'>
      <h3 style={styles.h3}>My Product</h3>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Username</th>
            <th>Mobile Number</th>
            <th>Email</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {homes.map((home) => {
            return (
              <tr>
                <td>{home.username}</td>
                <td>{home.mobileNo}</td>
                <td>{home.email}</td>
                <td>{home.address}</td>
                <td>
                
                  <button
                    onClick={() => editUser(home.username)}
                    style={styles.button}
                    className='btn btn-sm btn-success'>
                    Edit User
                  </button>
                  <button
                    onClick={() => deleteUser(home.username)}
                    style={styles.button}
                    className='btn btn-sm btn-danger'>
                    Delete User
                  </button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

const styles = {
    h3: {
      textAlign: 'center',
      margin: 20,
    },
    button: {
      marginRight: 10,
    },
  }

export default Home
