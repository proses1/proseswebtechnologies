import React from 'react';
import {BrowserRouter,Routes,Route,Link} from 'react-router-dom';
import Navbar from "./components/navbar";
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Home from "./pages/home";
import AddUser from "./pages/addUser";
import EditUser from "./pages/editUser";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar/>
          <Routes>
            <Route path='/home' element={<Home/>}/>
            <Route path='/addUser' element={<AddUser/>}/>
            <Route path='/editUser' element={<EditUser/>}/>
          </Routes>
          <ToastContainer position='top-right' autoClose={1000} />
      </BrowserRouter>
    
    </div>
  );
}

export default App;
